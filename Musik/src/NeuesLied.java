import java.awt.BorderLayout;
import java.awt.EventQueue;

import model.*;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import java.awt.GridLayout;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JSplitPane;

public class NeuesLied extends JFrame {

	private JPanel contentPane;
	private JTextField tfdTitel;
	private JTextField tfdBandname;
	private JTextField tfdGenre;
	
	private int nlI;
	
	public int getI() {
		return nlI;
	}

	public void setI(int i) {
		this.nlI = i;
	}

	protected static Song nlSong = new Song();
	protected Vote nlVote = new Vote(0);
	
	//protected JLabel lblTmp2;
	
	private final JLabel lblTmp2 = new JLabel("");
	/**
	 * @wbp.nonvisual location=551,39
	 */
	
	public String getLblTmp2() {
		return lblTmp2.getText();
	}
	
	public void setLblTmp2(String username) {
		this.lblTmp2.setText(username);
	}
	
	private JFrame selfNeuesLied;
	/**
	 * @wbp.nonvisual location=641,59
	 */
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					NeuesLied frame = new NeuesLied();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public NeuesLied() {
		this.selfNeuesLied = this;
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JLabel lblNeuesLiedEinfuegen = new JLabel("Neues Lied einfuegen");
		lblNeuesLiedEinfuegen.setHorizontalAlignment(SwingConstants.CENTER);
		contentPane.add(lblNeuesLiedEinfuegen, BorderLayout.NORTH);
		
		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.CENTER);
		panel.setLayout(new GridLayout(3, 3, 0, 0));
		
		JLabel lblTitel = new JLabel("Titel");
		panel.add(lblTitel);
		
		tfdTitel = new JTextField();
		panel.add(tfdTitel);
		tfdTitel.setColumns(10);
		
		JLabel lblBandname = new JLabel("Bandname");
		panel.add(lblBandname);
		
		tfdBandname = new JTextField();
		panel.add(tfdBandname);
		tfdBandname.setColumns(10);
		
		JLabel lblGenre = new JLabel("Genre");
		panel.add(lblGenre);
		
		tfdGenre = new JTextField();
		panel.add(tfdGenre);
		tfdGenre.setColumns(10);
		
		JSplitPane splitPane = new JSplitPane();
		contentPane.add(splitPane, BorderLayout.SOUTH);
		
		JLabel lblUsername = new JLabel();
		splitPane.setRightComponent(lblUsername);
		
		JButton btnEinfuegen = new JButton("OK");
		btnEinfuegen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				nlSong.setTitel(tfdTitel.getText());
				nlSong.setBandname(tfdBandname.getText());
				nlSong.setGenre(tfdGenre.getText());
				nlSong.setAnzahlgevoted(0);
				nlSong.eintrag();
				
				MusikVote.mvVote.setPerson(Login.lPersonen);
				MusikVote.mvVote.setSong(NeuesLied.nlSong);
				
				MusikVote musikVote = new MusikVote();
				MusikVote.mvVote.setVoteID(musikVote.getMvI());
				musikVote.setLblUsername2(lblTmp2.getText());
				musikVote.setMvI(nlI+1);
				musikVote.setVisible(true);
				selfNeuesLied.setVisible(false);
			}
		});
		splitPane.setLeftComponent(btnEinfuegen);
	}

}
