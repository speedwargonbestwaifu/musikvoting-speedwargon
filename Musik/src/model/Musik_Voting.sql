create DATABASE Musik_Voting ;
use Musik_Voting ;
CREATE TABLE `t_person` (
	`personid` INT(30) NOT NULL AUTO_INCREMENT,
	`anzahlvote` int(30) NOT NULL AUTO_INCREMENT,
	`p_name` VARCHAR(30) NULL DEFAULT NULL,
	PRIMARY KEY (`p_name`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
;
CREATE TABLE `t_song` (
	`p_songid` INT(11) NOT NULL AUTO_INCREMENT,
	`bandname` VARCHAR(30) NULL DEFAULT NULL,
	`genre` VARCHAR(30) NULL DEFAULT NULL,
	`titel` VARCHAR(30) NULL DEFAULT NULL,
	PRIMARY KEY (`p_songid`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
;
CREATE TABLE `t_voting` (
	`f_personid` INT(11) NULL DEFAULT NULL,
	`f_songid` INT(11) NULL DEFAULT NULL,
	INDEX `f_personid` (`f_personid`),
	INDEX `f_songid` (`f_songid`),
	CONSTRAINT `f_personid` FOREIGN KEY (`f_personid`) REFERENCES `t_person` (`p_name`),
	CONSTRAINT `f_songid` FOREIGN KEY (`f_songid`) REFERENCES `t_song` (`p_songid`)
)
ENGINE=InnoDB
;
SELECT P_name, personid, anzahlvoting from t_person;
SELECT P_songid,bandname,genre,titel from t_song;
SELECT F_personid,F_songid from t_voting;
SELECT * from t_voting full join t_person order by anzahlvoting asc limit 5;


