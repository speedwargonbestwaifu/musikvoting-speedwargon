package model;

public class Vote {

	private Personen person;
	private Song song;
	private int voteID;
	
	public Vote(Personen person, Song song, int voteID) {
		super();
		this.person = person;
		this.song = song;
		this.voteID = voteID;
	}
	
	public Vote(int voteID) {
		super();
		this.voteID = voteID;
	}
	
	public Vote() {
		super();
	}

	public Personen getPerson() {
		return person;
	}
	
	public void setPerson(Personen person) {
		this.person = person;
	}
	
	public Song getSong() {
		return song;
	}
	
	public void setSong(Song song) {
		this.song = song;
	}
	
	public int getVoteID() {
		return voteID;
	}

	public void setVoteID(int voteID) {
		this.voteID = voteID;
	}

	@Override
	public String toString() {
		return "Vote [" + person + ", " + song + "], voteID=" + voteID + "]";
	}

	public void voten(Personen person, Song song) {
		Vote vote = new Vote(person, song, voteID);
		person.setAnzahlvote(person.getAnzahlvote()+1);
		song.setAnzahlgevoted(song.getAnzahlgevoted()+1);
		System.out.println(vote.toString());
		voteID++;
	}
	
	public void votenAusgabe(Vote vote) {
		System.out.println(vote.toString());
	}
}

