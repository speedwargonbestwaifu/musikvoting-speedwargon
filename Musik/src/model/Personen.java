package model;

import java.util.Scanner;

public class Personen {
	
	private String name;
	private int anzahlvote;
	
	public Personen(String name, int anzahlvote) {
		super();
		this.name = name;
		this.anzahlvote = anzahlvote;
	}

	public Personen() {
		super();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public int getAnzahlvote() {
		return anzahlvote;
	}

	public void setAnzahlvote(int anzahlvote) {
		this.anzahlvote = anzahlvote;
	}

	@Override
	public String toString() {
		return "Personen [name=" + name + ", anzahlvote=" + anzahlvote + "]";
	}

	public void login() {
		Scanner dannyPersonen = new Scanner(System.in);
		
		//System.out.println("Name");
		//this.setName(dannyPersonen.nextLine());
		//int anzahlvote = 0;
		
		Personen personen = new Personen(name, anzahlvote);
		System.out.println(personen.toString());
		//System.out.println();
		
		//dannyPersonen.close();
	}
	
}

