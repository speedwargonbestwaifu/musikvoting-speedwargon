package model;

import java.util.Scanner;

public class Song {

	private String bandname;
	private String titel;
	private String genre;
	private int anzahlgevoted;
	
	public Song(String bandname, String titel, String genre, int anzahlgevoted) {
		super();
		this.bandname = bandname;
		this.titel = titel;
		this.genre = genre;
		this.anzahlgevoted = anzahlgevoted;
	}

	public Song() {
		super();
	}

	public String getBandname() {
		return bandname;
	}

	public void setBandname(String bandname) {
		this.bandname = bandname;
	}

	public String getTitel() {
		return titel;
	}

	public void setTitel(String titel) {
		this.titel = titel;
	}

	public String getGenre() {
		return genre;
	}

	public void setGenre(String genre) {
		this.genre = genre;
	}
	
	public int getAnzahlgevoted() {
		return anzahlgevoted;
	}

	public void setAnzahlgevoted(int anzahlgevoted) {
		this.anzahlgevoted = anzahlgevoted;
	}

	@Override
	public String toString() {
		return "Song [bandname=" + bandname + ", titel=" + titel + ", genre=" + genre + ", anzahlgevotet=" + anzahlgevoted + "]";
	}

	public void eintrag() {
		Scanner dannySong = new Scanner(System.in);
		
		//System.out.println("Bandname:");
		//this.setBandname(dannySong.nextLine());
		//System.out.println("Titel:");
		//this.setTitel(dannySong.nextLine());
		//System.out.println("Genre:");
		//this.setGenre(dannySong.nextLine());
		//int anzahlgevoted = 0;
		
		Song song = new Song(bandname, titel, genre, anzahlgevoted);
		System.out.println(song.toString());
		//System.out.println();
		
		//dannySong.close();
	}
	
}

