import java.awt.BorderLayout;
import java.awt.EventQueue;

import java.util.LinkedList;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import model.*;

import javax.swing.JLabel;
import java.awt.GridLayout;
import javax.swing.JTextField;
import javax.swing.JList;
import javax.swing.JTable;
import java.awt.Color;
import javax.swing.SwingConstants;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JSplitPane;

public class MusikVote extends JFrame {

	protected JPanel contentPane;
	protected JLabel lblUsername2;

	static int mvI;
	
	public int getMvI() {
		return mvI;
	}

	public void setMvI(int mvI) {
		this.mvI = mvI;
	}
	
	protected static Vote mvVote = new Vote(mvI);
	
	protected List<Vote> votes = new LinkedList<>();

	public List<Vote> getVotes() {
		return votes;
	}

	public void setVotes(List<Vote> votes) {
		this.votes = votes;
	}

	public boolean add(Vote vote) {
	return votes.add(vote);
	}
	
	public String getLblUsername2() {
		return lblUsername2.getText();
	}
	
	public void setLblUsername2(String username) {
		this.lblUsername2.setText(username);
	}

	private JFrame selfMusikVote;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MusikVote frame = new MusikVote();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MusikVote() {
		this.selfMusikVote = this;
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JLabel lblMusikVote = new JLabel("Musik Vote");
		lblMusikVote.setHorizontalAlignment(SwingConstants.CENTER);
		contentPane.add(lblMusikVote, BorderLayout.NORTH);
		
		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.CENTER);
		panel.setLayout(new GridLayout(1, 3, 0, 0));
		
		JList list = new JList();
		list.setModel(new DefaultListModel<>());
		panel.add(list);

		//-----------------------------------------------------------------------------------Voten		
		
		JButton btnVoten = new JButton("Voten");
		btnVoten.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Login.lPersonen.setAnzahlvote(Login.lPersonen.getAnzahlvote()+1);
				NeuesLied.nlSong.setAnzahlgevoted(NeuesLied.nlSong.getAnzahlgevoted()+1);
				mvVote.votenAusgabe(mvVote);
				
				setMvI(mvVote.getVoteID()+1);
				mvVote.setVoteID(mvI);
			}
		});
		contentPane.add(btnVoten, BorderLayout.EAST);
		
		JPanel panel_1 = new JPanel();
		contentPane.add(panel_1, BorderLayout.SOUTH);
		panel_1.setLayout(new GridLayout(1, 3, 0, 0));
		
		JButton btnNeuesLied = new JButton("Neues Lied");
		btnNeuesLied.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				NeuesLied neuesLied = new NeuesLied();
				neuesLied.setLblTmp2(lblUsername2.getText());
				neuesLied.setI(mvVote.getVoteID());
				neuesLied.setVisible(true);
				selfMusikVote.setVisible(false);
			}
		});
		panel_1.add(btnNeuesLied);
		
		lblUsername2 = new JLabel();
		lblUsername2.setHorizontalAlignment(SwingConstants.CENTER);
		panel_1.add(lblUsername2);
		
		JButton btnAusloggen = new JButton("Ausloggen");
		btnAusloggen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setMvI(0);
				
				Login login = new Login();
				login.setVisible(true);
				
				selfMusikVote.setVisible(false);
			}
		});
		panel_1.add(btnAusloggen);
	}

}
